// pages/book/book.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        bookList:[{
            "rebate_event": false,
            "rating": 4.6,
            "kind": "",
            "fixed_price": 7800,
            "subtitle": "匡扶摇漫画作品集",
            "title": "纳闷集",
            "abstract": "后现代都市幽默，漫画市井长诗\r\n继《回答不了》后匡扶摇全新作品集，百万读者催更的原创漫画\r\n●触动百万读者，创造刷屏记录的原创漫画家匡扶，最新治愈系漫画作品集，…",
            "author": "匡扶 绘著",
            "cover": "https://img2.doubanio.com\/view\/ark_article_cover\/retina\/public\/165430291.jpg?v=1606982259",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 55,
            "promotion": true,
            "price": 1299,
            "type": "ebook",
            "id": "165430291",
            "cover_label": "sale-label"
        }, {
            "rebate_event": false,
            "rating": 4.5,
            "kind": "",
            "fixed_price": 1995,
            "subtitle": "其实是一本严谨的极简中国史",
            "title": "半小时漫画中国史（修订版）",
            "abstract": "仅仅通过手绘和段子，二混子就捋出清晰的历史大脉络：春秋战国像个班级、大秦过把瘾就死、三国就三大战役、魏晋乱世多奇葩、南北朝盛产败家子、大唐最炫民族风……掀开装模…",
            "author": "二混子",
            "cover": "https://img9.doubanio.com\/view\/ark_article_cover\/retina\/public\/35537226.jpg?v=0",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 235,
            "promotion": true,
            "price": 899,
            "type": "ebook",
            "id": "35537226",
            "cover_label": "sale-label"
        }, {
            "rebate_event": false,
            "rating": 3.9,
            "kind": "",
            "fixed_price": 3360,
            "subtitle": "",
            "title": "如何孤独终老",
            "abstract": "如果你经常感到社恐，觉得刷手机比社交OR谈恋爱更有意思；如果工作日你只想躺在床上当咸鱼；如果你办了健身卡却只去过一次健身房；如果你热爱外卖、薯片和可乐；如果你可…",
            "author": "[美] 莫·韦尔奇 著\/绘",
            "cover": "https://img1.doubanio.com\/view\/ark_article_cover\/retina\/public\/346171178.jpg?v=1644910239",
            "is_bundle": false,
            "translator": "吴华",
            "rating_cnt": 10,
            "promotion": true,
            "price": 2016,
            "type": "ebook",
            "id": "346171178",
            "cover_label": "limited-vip-can-read-label"
        }, {
            "rebate_event": false,
            "rating": 4.5,
            "kind": "",
            "fixed_price": 2500,
            "subtitle": "高木直子全家吃饱饱万岁！",
            "title": "不再是一个人吃饭啦",
            "abstract": "全家吃饱饱万岁！！\r\n高木老师41岁的时候结婚啦！开始了和丈夫一起的夫妇饮食。两个人会一起逛超市，一起在厨房忙忙碌碌，美好而又惬意。\r\n后来高木老师怀孕啦！丈夫…",
            "author": "[日] 高木直子 著绘",
            "cover": "https://img1.doubanio.com\/view\/ark_article_cover\/retina\/public\/327617518.jpg?v=1629094618",
            "is_bundle": false,
            "translator": "香冰",
            "rating_cnt": 58,
            "promotion": false,
            "price": 2500,
            "type": "ebook",
            "id": "327617518",
            "cover_label": "vip-can-read-label"
        }, {
            "rebate_event": false,
            "rating": 0.0,
            "kind": "",
            "fixed_price": 3299,
            "subtitle": "豆瓣评分9.0近500人标记，中文读者翘首以盼，风靡欧美的动漫画工作坊经典教科书，呈现讲述同一个故事的99种“脑洞”",
            "title": "一个故事的99种讲法",
            "abstract": "这是知名漫画家马特·马登基于雷蒙·格诺的名作《风格练习》创作的漫画，以99种不同的漫画风格或形式讲述同一个简单的故事。它开发了讲故事的诸多可能性，每一种风格都是…",
            "author": "[美] 马特·马登",
            "cover": "https://img1.doubanio.com\/view\/ark_article_cover\/retina\/public\/381207589.jpg?v=1647407308",
            "is_bundle": false,
            "translator": "黄远帆",
            "rating_cnt": 1,
            "promotion": true,
            "price": 2380,
            "type": "ebook",
            "id": "381207589",
            "cover_label": "sale-label"
        }, {
            "rebate_event": false,
            "rating": 4.6,
            "kind": "",
            "fixed_price": 4299,
            "subtitle": "",
            "title": "生活蒙太奇",
            "abstract": "本书是一部充满奇妙质感的绘本作品，每天都有无数年轻人转发或评论它的精彩画面或动图。#生活蒙太奇#在全球社交媒体上有超过1345万以上的话题阅读（包括但不限于新浪…",
            "author": "天然 绘著",
            "cover": "https://img2.doubanio.com\/view\/ark_article_cover\/retina\/public\/157679211.jpg?v=1598602476",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 186,
            "promotion": true,
            "price": 2579,
            "type": "ebook",
            "id": "157679211",
            "cover_label": "vip-can-read-label"
        }, {
            "rebate_event": false,
            "rating": 3.9,
            "kind": "",
            "fixed_price": 4400,
            "subtitle": "",
            "title": "胖虎下山",
            "abstract": "虎出没，请注意！“猫铃铛”作者新作，漫画家、玩具设计师@不二马大叔，个人画集欢喜参上！\r\n胖虎是不二马于2017年创作的动物形象，2020年1月底，在微博中上山…",
            "author": "不二马",
            "cover": "https://img1.doubanio.com\/view\/ark_article_cover\/retina\/public\/337114609.jpg?v=1640749221",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 15,
            "promotion": false,
            "price": 4400,
            "type": "ebook",
            "id": "337114609",
            "cover_label": "limited-vip-can-read-label"
        }, {
            "rebate_event": false,
            "rating": 0.0,
            "kind": "",
            "fixed_price": 4980,
            "subtitle": "",
            "title": "镖人11",
            "abstract": "◆轰动日本的中国漫画《镖人》单行本第11册！\r\n◆脱口秀大师李诞、实力演员万茜、知名作家马伯庸、知名动画导演田晓鹏鼎力推荐！\r\n◆3次登上日本央视NHK电视台新…",
            "author": "许先哲",
            "cover": "https://img9.doubanio.com\/view\/ark_article_cover\/retina\/public\/380327705.jpg?v=1646732265",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 2,
            "promotion": true,
            "price": 1499,
            "type": "ebook",
            "id": "380327705",
            "cover_label": "vip-can-read-label"
        }, {
            "rebate_event": false,
            "rating": 4.8,
            "kind": "",
            "fixed_price": 999,
            "subtitle": "",
            "title": "尾文字鱼",
            "abstract": "本书收录2012~2015微博连载的所有《尾文字鱼》系列漫画，包括：\r\n《灵光一闪的银龙鱼》\r\n《回转待食的三文鱼》\r\n《困于渔网中的鲫鱼》\r\n《不是你爸爸的鳄…",
            "author": "伍肆",
            "cover": "https://img3.doubanio.com\/view\/ark_article_cover\/retina\/public\/42435300.jpg?v=0",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 443,
            "promotion": false,
            "price": 999,
            "type": "ebook",
            "id": "42435300",
            "cover_label": ""
        }, {
            "rebate_event": false,
            "rating": 4.5,
            "kind": "",
            "fixed_price": 2999,
            "subtitle": "Buddy Gator盖朵短尾鳄疗愈漫画！",
            "title": "看你一眼就会笑",
            "abstract": "小鳄鱼盖朵和他可爱的小伙伴们的疗愈故事！\r\n趣味暖心漫画，或温馨、或可爱、或搞笑、或黑色幽默……集创意脑洞和可爱疗愈于一体，CHOW希望能传递温暖善意、开心快乐…",
            "author": "[马来西亚] 丘汉林",
            "cover": "https://img1.doubanio.com\/view\/ark_article_cover\/retina\/public\/330447269.jpg?v=1632897299",
            "is_bundle": false,
            "translator": "",
            "rating_cnt": 57,
            "promotion": false,
            "price": 2999,
            "type": "ebook",
            "id": "330447269",
            "cover_label": "vip-can-read-label"
        }]

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})